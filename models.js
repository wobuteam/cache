/**
*	All the cache models.
*/

var	config = require('./config.json'),
	mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/'+config.database);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
	
	console.log('Hello database! Connected to '+database+'.');
	
	var ItemSchema = new mongoose.Schema({
		key : String,
		value : String,
		expires : {
			index : true,
			type : Number
		}
	});
	
	exports.Item = mongoose.model('Item', ItemSchema);
	
});
