#!/bin/bash
# A simple script to control running this NodeJS server!

TIMESTAMP=$(date +"%s")

BASE=`readlink -f "$0"`
BASE="`dirname "$BASE"`/"
pushd "$BASE"

forever stop dillingercache.js
mv ~/.forever/forever-dillingercache.log ~/.forever/logs/dillingercache.$TIMESTAMP.log
mkdir forever/$TIMESTAMP -p
mv forever/*.log forever/$TIMESTAMP/
forever start -l forever-dillingercache.log -o forever/out.log -e forever/error.log dillingercache.js

popd