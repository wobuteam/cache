/**
 *	Caching System on Dillinger
 */

var express = require('express'),
	http = require('http'),
	model = require('./models'),
	path = require('path'),
	
	app = express();

app.set('port', process.env.PORT || 3001);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);

if (process.env.NODE_ENV == 'development')
	app.use(express.errorHandler());


app.get('/purge', function(req, res) {
	model.Item.remove({ expires : { $lt: Date.now() } }, function(err) {
		if (err)
			res.jsonp(400);
		else
			res.jsonp(204);
	});
});

app.get('/', function(req, res) {
	if (req.query.key)
		model.Item.findOne({ key : req.query.key }, function(err, item) {
			if (err)
				res.jsonp(404, {});
			else
				res.jsonp(200, item);
		});
	else
		res.jsonp(404, {});
});

app.post('/', function(req, res) {
	console.log(req.body);
	if (req.body.key && req.body.value) {
		var expires;
		if (!req.body.expires)
			req.body.expires = 86400; // 24 hours
		model.Item.create(
			{
				key : req.body.key,
				value : req.body.value,
				expires : req.body.expires
			},
			function(err, item) {
				if (err)
					res.jsonp(400, {});
				else
					res.jsonp(201, item);
			}
		);
	}
	else
		res.jsonp(400, {});
});

app.delete('/', function(req, res) {
	if (req.query.key)
		model.Item.remove({ key : req.query.key }, function(err) {
			if (err)
				res.jsonp(400, {});
			else
				res.jsonp(204);
		});
	else
		res.jsonp(400);
});


http.createServer(app).listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
});